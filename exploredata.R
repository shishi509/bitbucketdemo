# to inspect the data
head(iris)
summary(iris)

# update names for easiler indexing
names(iris) = c("Sepal_Length",  "Sepal_Width",   "Petal_Length",  "Petal_Width",   "Species")